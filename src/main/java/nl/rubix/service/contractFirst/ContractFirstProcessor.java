package nl.rubix.service.contractFirst;


import java.io.InputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.wsdl.Message;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.Node;

import nl.rubix.service.xsd.neworder.CustomerType;
import nl.rubix.service.xsd.neworder.GetOrderRequest;
import nl.rubix.service.xsd.neworder.GetOrderResponse;
import nl.rubix.service.xsd.neworder.ObjectFactory;
import nl.rubix.service.xsd.neworder.OrderListType;
import nl.rubix.service.xsd.neworder.OrderType;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.sun.xml.bind.v2.runtime.JAXBContextImpl.JAXBContextBuilder;

public class ContractFirstProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		GetOrderRequest input =  exchange.getIn().getBody(GetOrderRequest.class);
		
		Map<String, Object> headers = exchange.getIn().getHeaders();
		for (Map.Entry<String, Object> header : headers.entrySet()){
			System.out.println("headers: " + header.getKey() + "  " + header.getValue() );
		}
		
		
		
		
		GetOrderResponse outbody = mapOutputMessage(input);
		
		exchange.getOut().setBody(outbody);
	}

	private GetOrderResponse mapOutputMessage(GetOrderRequest input) {
		ObjectFactory objFac = new ObjectFactory();
		GetOrderResponse outbody = objFac.createGetOrderResponse();
		outbody.setOrderNumber(input.getOrderNumber());
		CustomerType newCustomer = new CustomerType();
		newCustomer.setName("Dirk");
		newCustomer.setLastname("Janssen");
		outbody.setCustomer(newCustomer);

		OrderType order = new OrderType();
		order.setPrice(12);
		order.setProductName("TV");
		
		OrderListType newOrderList = new OrderListType();
		
		newOrderList.getOrder().add(order);
		outbody.setOrderList(newOrderList);
		return outbody;
	}

	
}
